<!DOCTYPE html>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
<meta charset="UTF-8">
<title><spring:message code="changepwd.title" /></title>
</head>
<body>
	<div class="center">
		<p>
			<spring:message code="changepwd.header" />
		</p>
		<form:form commandName="changepwd" method="POST" autocomplete="off"
			action="${pageContext.request.contextPath}/changepwd"
			id="changepwd-user-form" name="changepwdform"
			class="pure-form pure-form-aligned">
			<fieldset>
                <div class="pure-control-group">
                    <Label><spring:message code="changepwd.username" /></Label>
                    <form:input path="userName" type="text" size="32" maxlength="32" disabled="true" />
                </div>
                <div class="pure-control-group">
                    <Label><spring:message code="changepwd.currpwd" /></Label>
                    <form:input path="currentPassword" type="password" size="32" maxlength="32" />
                </div>
                <div class="pure-control-group">
                    <Label><spring:message code="changepwd.password" /></Label>
                    <form:input path="password" type="password" size="32" maxlength="32" />
                </div>
                <div class="pure-control-group">
                    <Label><spring:message code="changepwd.repeatedPassword" /></Label>
                    <form:input path="repeatedPassword" type="password" size="32" maxlength="32" />
                </div>
                <div class="pure-control-group">
                    <Label><spring:message code="changepwd.tzone" /></Label>
                    <form:select path="tzone" items="${tzones}" />
                </div>
 				<div class="pure-controls">
                                    <p><span class="error"><form:errors path="*" /></span></p>
 				    <button class="pure-button pure-button-primary button-small" type="submit">
					    <spring:message code="changepwd.button"/></button>
                </div>
			</fieldset>
		</form:form>
	</div>
	<script type="text/javascript">
	    document.getElementById('currentPassword').focus();
	    var ajaxFormName = '#changepwd-user-form';
	    var ajaxUrl = '${pageContext.request.contextPath}/changepwd.json';
	</script>
	<script src="${pageContext.request.contextPath}/webjarslocator/jquery/jquery.min.js"
        type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/js/common.js"
        type="text/javascript"></script>
</body>
</html>
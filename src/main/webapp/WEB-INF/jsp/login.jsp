<!DOCTYPE html>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
<meta charset="UTF-8">
<title><spring:message code="login.title" /></title>
</head>
<body>
	<div class="center">
			<div><spring:message code="login.header" /></div>
		<form:form commandName="login" method="POST" autocomplete="off"
			action="${pageContext.request.contextPath}/login"
			id="login-user-form" name="loginform"
			cssClass="pure-form pure-form-aligned">
			<fieldset>
			    <div class="pure-control-group">
				    <Label><spring:message code="login.username" /></Label>
				    <form:input path="userName" type="text" size="32" maxlength="32" />
				</div>
				<div class="pure-control-group">
				    <Label><spring:message code="login.password" /></Label>
				    <form:input path="password" type="password" size="32" maxlength="32" />
				</div>
				<div class="pure-controls">
                                    <p><span class="error"><form:errors path="*" /></span></p>
				    <button class="pure-button pure-button-primary button-small" type="submit"><spring:message code="login.button"/></button>
	                <p><spring:message code="login.register" />
					<br /><a href="${pageContext.request.contextPath}/register" class="pure-button button-small"><spring:message
					code="login.reglink" /></a></p>
				</div>
			</fieldset>
		</form:form>

	</div>
	<script type="text/javascript">
	    document.getElementById('userName').focus();
	    var ajaxFormName = '#login-user-form';
	    var ajaxUrl = '${pageContext.request.contextPath}/login.json';
	</script>
	<script src="${pageContext.request.contextPath}/webjarslocator/jquery/jquery.min.js"
        type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/js/common.js"
        type="text/javascript"></script>
</body>
</html>
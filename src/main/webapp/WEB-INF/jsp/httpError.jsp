
<!DOCTYPE html>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head><meta charset="UTF-8" /><title><spring:message code="error.title" /></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<spring:theme var="theme" code="name" />
<link rel="shortcut icon"
    href="${pageContext.request.contextPath}/images/favicon.ico"
    type="image/x-icon" />
<link type="text/css" rel="stylesheet"
    href="${pageContext.request.contextPath}/webjarslocator/pure/pure-min.css" />
<link type="text/css" rel="stylesheet"
    href="${pageContext.request.contextPath}/webjarslocator/font-awesome/css/font-awesome.min.css" />
<link type="text/css" rel="stylesheet"
    href="${pageContext.request.contextPath}/css/style.css" />
<link type="text/css" rel="stylesheet"
    href="${pageContext.request.contextPath}/css/style-${theme}.css" />
</head>
<body>
    <div class="center pure-menu pure-menu-horizontal">
        <a href="#" class="pure-menu-heading"> <span class="bold"><spring:message
                    code="decorator.header" /> <i
                class="fa fa-money fa-lg"></i></span>
        </a>
    </div>
<div class="center">
    <h1><spring:message code="error.header" /></h1>
    <p><spring:message code="httpError.text" /></p>
    <p><spring:message code="httpError.status" /> ${status}</p>
    <p><spring:message code="httpError.reason" /> ${reason}</p>
</div>
    <div class="center">
        <p>
            <spring:message code="decorator.footer" />
        </p>
    </div>
</body>
</html>
<!DOCTYPE html>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
<meta charset="UTF-8">
<title><spring:message code="register.title" /></title>
</head>
<body>
	<div class="center">
		<p>
			<spring:message code="register.header" />
		</p>
		<form:form commandName="register" method="POST" autocomplete="off"
			action="${pageContext.request.contextPath}/register"
			id="register-user-form" name="registerform"
			class="pure-form pure-form-aligned">
			<fieldset>
			    <div class="pure-control-group">
				    <Label><spring:message code="register.username" /></Label>
				    <form:input path="userName" type="text" size="32" maxlength="32" />
				</div>
                <div class="pure-control-group">
                    <Label><spring:message code="register.password" /></Label>
                    <form:input path="password" type="password" size="32" maxlength="32" />
                </div>
                <div class="pure-control-group">
                    <Label><spring:message code="register.repeatedPassword" /></Label>
                    <form:input path="repeatedPassword" type="password" size="32" maxlength="32" />
                </div>
                 <div class="pure-control-group">
                    <Label><spring:message code="register.tzone" /></Label>
                    <form:select path="tzone" items="${tzones}" />
                </div>
				<div class="pure-controls">
                                    <p><span class="error"><form:errors path="*" /></span></p>
 				    <button class="pure-button pure-button-primary button-small" type="submit">
					    <spring:message code="register.button"/></button>
                </div>
			</fieldset>
		</form:form>
	</div>
	<script type="text/javascript">
	    document.getElementById('userName').focus();
	    var ajaxFormName = '#register-user-form';
	    var ajaxUrl = '${pageContext.request.contextPath}/register.json';
	</script>
	<script src="${pageContext.request.contextPath}/webjarslocator/jquery/jquery.min.js"
        type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/js/common.js"
        type="text/javascript"></script>
</body>
</html>
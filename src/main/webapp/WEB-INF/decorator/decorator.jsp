<!DOCTYPE html>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<title><sitemesh:write property='title' /></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<spring:theme var="theme" code="name" />
<link rel="shortcut icon"
    href="${pageContext.request.contextPath}/images/favicon.ico"
    type="image/x-icon" />
<link type="text/css" rel="stylesheet"
    href="${pageContext.request.contextPath}/webjarslocator/pure/pure-min.css" />
<link type="text/css" rel="stylesheet"
    href="${pageContext.request.contextPath}/webjarslocator/font-awesome/css/font-awesome.min.css" />
<link type="text/css" rel="stylesheet"
    href="${pageContext.request.contextPath}/css/style.css" />
<link type="text/css" rel="stylesheet"
    href="${pageContext.request.contextPath}/css/style-${theme}.css" />
<style id="antiClickjack">body{display:none !important;}</style>
<script type="text/javascript">
   if (self === top) {
       var antiClickjack = document.getElementById("antiClickjack");
       antiClickjack.parentNode.removeChild(antiClickjack);
   } else {
       top.location = self.location;
   }
</script>
</head>
<body>
    <div class="center pure-menu pure-menu-horizontal">
        <a href="#" class="pure-menu-heading"> <span class="bold"><spring:message
                    code="decorator.header" /> <i
                class="fa fa-money fa-lg"></i></span>
        </a>
        <ul  class="pure-menu-list">
            <li class="pure-menu-item"><c:if test="${theme == 'blue'}">
                    <a href="?theme=black" class="pure-menu-link"><spring:message
                            code="decorator.theme.black" /></a>
                </c:if>
                <c:if test="${theme == 'black'}">
                    <a href="?theme=blue" class="pure-menu-link"><spring:message
                            code="decorator.theme.blue" /></a>
                </c:if>
            </li>
            <c:if test="${! empty USER}">
                <li class="pure-menu-item"><a href="${pageContext.servletContext.contextPath}/changepwd" class="pure-menu-link">
                        <spring:message code="decorator.changepwd" /></a>
                </li>
                <li class="pure-menu-item"><a href="${pageContext.servletContext.contextPath}/logout" class="pure-menu-link">
                        <spring:message code="decorator.logout" /></a>
                </li>
            </c:if>
        </ul>
    </div>
    <sitemesh:write property='body' />
    <div class="center">
        <p>
            <spring:message code="decorator.footer" />
        </p>
    </div>
</body>
</html>
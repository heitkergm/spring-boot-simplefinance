package com.dappermoose.sbsimplefinance.data;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 * The BaseEntity class.
 */
@MappedSuperclass
public abstract class AbstractBaseModifiableEntity extends AbstractBaseEntity
{
    private static final long serialVersionUID = 1446802725141477516L;
    
    // database will set a default value
    @Column (name = "MODIFIED_AT", nullable = false)
    private Instant modified;

    /**
     * Sets the timestamps.
     * <p>
     * Ensure that the time stored is ALWAYS GMT
     *</p>
     */
    @Override
    @PrePersist
    @PreUpdate
    public void setTimestamps ()
    {
        super.setTimestamps ();
        if (modified == null)
        {
            modified = getCreated ();
        }
        else
        {
            modified = Instant.now ();
        }
    }

    /**
     * Gets the modified.
     *
     * @return the modified
     */
    public Instant getModified ()
    {
        return modified;
    }

    /**
     * Sets the modified.
     *
     * @param modifiedNew the new modified
     */
    public void setModified (final Instant modifiedNew)
    {
        modified = modifiedNew;
    }
}

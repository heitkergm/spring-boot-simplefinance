package com.dappermoose.sbsimplefinance.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Check;

// TODO: Auto-generated Javadoc
/**
 * The Class LoginEvent.
 */
@Entity
@Table (name = "LOGIN_EVENT")
@Check (constraints = "SUCCESS IN ('Y', 'N')")
public class LoginEvent extends AbstractBaseEntity
{
    private static final long serialVersionUID = -5740191601882965493L;

    @Id
    @GeneratedValue (strategy = GenerationType.SEQUENCE, generator = "LOGIN_EVENT_ID_SEQ")
    @SequenceGenerator (name = "LOGIN_EVENT_ID_SEQ", sequenceName = "LOGIN_EVENT_ID_SEQ", allocationSize = 1)
    @Column (name = "LOGIN_EVENT_ID", nullable = false, updatable = false)
    private Long loginEventId;

    @ManyToOne (optional = false)
    @JoinColumn (name = "USER_ID", nullable = false, updatable = false, foreignKey = @ForeignKey (name = "FK_LOGIN_EVENT_USER"))
    private User user;

    @Column (name = "SUCCESS", nullable = false, length = 1, updatable = false)
    private YesNoEnum success;

    /**
     * Gets the login event id.
     *
     * @return the login event id
     */
    public Long getLoginEventId ()
    {
        return loginEventId;
    }

    /**
     * Sets the login event id.
     *
     * @param loginEventIdNew the new login event id
     */
    public void setLoginEventId (final Long loginEventIdNew)
    {
        loginEventId = loginEventIdNew;
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    public User getUser ()
    {
        return user;
    }

    /**
     * Sets the user.
     *
     * @param userNew the new user
     */
    public void setUser (final User userNew)
    {
        user = userNew;
    }

    /**
     * Gets the success string.
     *
     * @return the password
     */
    public YesNoEnum getSuccess ()
    {
        return success;
    }

    /**
     * Sets the success.
     *
     * @param successNew the new success
     */
    public void setSuccess (final YesNoEnum successNew)
    {
        success = successNew;
    }
}

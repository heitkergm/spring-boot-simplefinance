package com.dappermoose.sbsimplefinance.formbean;

// TODO: Auto-generated Javadoc
/**
 * The Class BaseFormBean.
 */
public class BaseFormBean
{

    /** The nonce. */
    private String nonce;

    /**
     * Gets the nonce.
     *
     * @return the nonce
     */
    public String getNonce ()
    {
        return nonce;
    }

    /**
     * Sets the nonce.
     *
     * @param nonceNew the new nonce
     */
    public void setNonce (final String nonceNew)
    {
        nonce = nonceNew;
    }
}

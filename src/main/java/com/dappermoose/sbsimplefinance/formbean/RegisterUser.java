package com.dappermoose.sbsimplefinance.formbean;

import javax.validation.constraints.Size;

import com.dappermoose.sbsimplefinance.validation.PasswordMatch;

// TODO: Auto-generated Javadoc
/**
 * The Class RegisterUser.
 */
@PasswordMatch (password = "password", repassword = "repeatedPassword", message = "{register.pwd.notmatch}")
public class RegisterUser extends LoginUser
{
    /** The repeated password. */
    @Size (min = 1, max = 32, message = "{register.secondPassword.size}")
    private String repeatedPassword;
    
    /** the time zone. */
    @Size (min = 1, max = 128, message = "{register.tzone.size}")
    private String tzone;

    /**
     * Gets the repeated password.
     *
     * @return the repeated password
     */
    public String getRepeatedPassword ()
    {
        return repeatedPassword;
    }

    /**
     * Sets the repeated password.
     *
     * @param repeatedPasswordNew the new repeated password
     */
    public void setRepeatedPassword (final String repeatedPasswordNew)
    {
        repeatedPassword = repeatedPasswordNew;
    }
    
    /**
     * Get the time zone.
     * 
     * @return the time zone
     */
    public String getTzone ()
    {
        return tzone;
    }

    /**
     * Set the time zone.
     * 
     * @param tzoneNew the new value for the time zone 
     */
    public void setTzone (final String tzoneNew)
    {
        tzone = tzoneNew;
    }
}

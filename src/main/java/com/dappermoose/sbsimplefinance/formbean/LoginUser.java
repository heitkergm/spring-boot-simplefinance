package com.dappermoose.sbsimplefinance.formbean;

import javax.validation.constraints.Size;

// TODO: Auto-generated Javadoc
/**
 * The Class LoginUser.
 */

public class LoginUser extends BaseFormBean
{
    @Size (min = 1, max = 32, message = "{login.user.size}")
    private String userName;

    // the password
    @Size (min = 1, max = 32, message = "{login.password.size}")
    protected String password;

    /**
     * Gets the user name.
     *
     * @return the user name
     */
    public String getUserName ()
    {
        return userName;
    }

    /**
     * Sets the user name.
     *
     * @param userNameNew the new user name
     */
    public void setUserName (final String userNameNew)
    {
        userName = userNameNew;
    }

    /**
     * Gets the password.
     *
     * @return the password
     */
    public String getPassword ()
    {
        return password;
    }

    /**
     * Sets the password.
     *
     * @param passwordNew the new password
     */
    public void setPassword (final String passwordNew)
    {
        password = passwordNew;
    }
}

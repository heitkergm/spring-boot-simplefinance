package com.dappermoose.sbsimplefinance.util;

import java.util.UUID;

import javax.servlet.http.HttpSession;

// TODO: Auto-generated Javadoc
/**
 * The Class CSRFToken.
 */
public final class CSRFToken
{

    /** The csrf field name. */
    public static final String CSRF_FIELD_NAME = "nonce";

    /**
     * Gets the token.
     *
     * @param session
     *            the session
     * @return the token
     */
    public static String getToken (final HttpSession session)
    {
        String tokenValue = (String) (session.getAttribute (CSRF_FIELD_NAME));
        if (tokenValue == null)
        {
            tokenValue = UUID.randomUUID ().toString ();
            session.setAttribute (CSRF_FIELD_NAME, tokenValue);
        }
        return tokenValue;
    }

    private CSRFToken ()
    {
    }
}

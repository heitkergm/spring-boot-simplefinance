package com.dappermoose.sbsimplefinance.util;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.support.RequestDataValueProcessor;

// TODO: Auto-generated Javadoc
/**
 * The Class HiddenFields.
 */
public class HiddenFields implements RequestDataValueProcessor
{

    /* (non-Javadoc)
     * @see org.springframework.web.servlet.support.RequestDataValueProcessor#getExtraHiddenFields(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public Map<String, String>
            getExtraHiddenFields (final HttpServletRequest request)
    {
        final HashMap<String, String> retVal = new HashMap<> ();
        retVal.put (CSRFToken.CSRF_FIELD_NAME,
                CSRFToken.getToken (request.getSession ()));
        return retVal;
    }

    /* (non-Javadoc)
     * @see org.springframework.web.servlet.support.RequestDataValueProcessor#processAction(javax.servlet.http.HttpServletRequest, java.lang.String, java.lang.String)
     */
    @Override
    public String
            processAction (final HttpServletRequest request,
                    final String action, final String httpMethod)
    {
        return action;
    }

    /* (non-Javadoc)
     * @see org.springframework.web.servlet.support.RequestDataValueProcessor#processFormFieldValue(javax.servlet.http.HttpServletRequest, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public String processFormFieldValue (final HttpServletRequest request,
            final String name,
            final String value,
            final String type)
    {
        return value;
    }

    /* (non-Javadoc)
     * @see org.springframework.web.servlet.support.RequestDataValueProcessor#processUrl(javax.servlet.http.HttpServletRequest, java.lang.String)
     */
    @Override
    public String
            processUrl (final HttpServletRequest request, final String url)
    {
        return url;
    }

}

package com.dappermoose.sbsimplefinance.validation;

import java.util.List;

import org.springframework.validation.ObjectError;

// TODO: Auto-generated Javadoc
/**
 * The Class ValidationResponse.
 */
public class ValidationResponse
{

    /** The status. */
    private String status;

    /** The error message list. */
    private List<ObjectError> errorMessageList;

    /**
     * Gets the status.
     *
     * @return the status
     */
    public String getStatus ()
    {
        return status;
    }

    /**
     * Sets the status.
     *
     * @param statusNew the new status
     */
    public void setStatus (final String statusNew)
    {
        status = statusNew;
    }

    /**
     * Gets the error message list.
     *
     * @return the error message list
     */
    public List<ObjectError> getErrorMessageList ()
    {
        return errorMessageList;
    }

    /**
     * Sets the error message list.
     *
     * @param errorMessageListNew the new error message list
     */
    public void setErrorMessageList (final List<ObjectError> errorMessageListNew)
    {
        errorMessageList = errorMessageListNew;
    }
}

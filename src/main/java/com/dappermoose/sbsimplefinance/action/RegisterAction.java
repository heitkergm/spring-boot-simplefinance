package com.dappermoose.sbsimplefinance.action;

import java.util.List;
import java.util.TimeZone;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dappermoose.sbsimplefinance.dao.UserRepository;
import com.dappermoose.sbsimplefinance.data.User;
import com.dappermoose.sbsimplefinance.formbean.RegisterUser;
import com.dappermoose.sbsimplefinance.validation.ValidationResponse;

// TODO: Auto-generated Javadoc
/**
 * The Class LoginAction.
 */
@Controller
public class RegisterAction
{

    /** The user repository. */
    @Inject
    private UserRepository userRepository;

    /** The message source. */
    @Inject
    private MessageSource messageSource;

    @Inject
    private ApplicationContext context;
    
    /**
     * Main action. Display the form.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping (value = "/register", method = RequestMethod.GET)
    public String mainAction (final Model model)
    {
        RegisterUser ru = new RegisterUser ();
        ru.setTzone (TimeZone.getDefault ().getID ());
        model.addAttribute ("register", ru);
        model.addAttribute ("tzones", context.getBean ("tzones"));
        return "register";
    }

    /**
     * handle the ajax validation.
     *
     * @param register the register
     * @param result the result
     * @param request the request
     * @return the validation response
     */
    @RequestMapping (value = "/register.json")
    @ResponseBody
    public ValidationResponse validationAction (
            @RequestBody @Valid final RegisterUser register,
            final BindingResult result, final HttpServletRequest request)
    {
        final ValidationResponse resp = new ValidationResponse ();
        if (!result.hasErrors ())
        {
            // check if the user already exists
            final List<User> users = userRepository.findByUserName (register
                    .getUserName ());
            if (users.size () > 0)
            {
                final ObjectError objErr = new ObjectError ("RegisterUser",
                        messageSource.getMessage ("register.duplicateUser",
                                null, request.getLocale ()));
                result.addError (objErr);
            }
        }
        if (result.hasErrors ())
        {
            resp.setStatus ("FAIL");
            resp.setErrorMessageList (result.getAllErrors ());
        }
        else
        {
            resp.setStatus ("SUCCESS");
        }
        return resp;
    }

    /**
     * Process register action.
     *
     * @param register the register
     * @param res the res
     * @param model the model
     * @param request the request
     * @return the string
     */
    @Transactional
    @NeedsCSRFCheck
    @RequestMapping (value = "/register", method = RequestMethod.POST)
    public String processRegisterAction (
            @Valid @ModelAttribute ("register") final RegisterUser register,
            final BindingResult res, final Model model,
            final HttpServletRequest request)
    {
        if (res.hasErrors ())
        {
            model.addAttribute ("register", register);
            return "register";
        }

        // check if the user already exists
        final List<User> users = userRepository.findByUserName (register
                .getUserName ());
        if (users.size () > 0)
        {
            final ObjectError objErr = new ObjectError ("RegisterUser",
                    messageSource.getMessage ("register.duplicateUser", null,
                            request.getLocale ()));
            res.addError (objErr);
            model.addAttribute ("register", register);
            return "register";
        }

        // create user bean
        final User user = new User ();
        user.setUserName (register.getUserName ());
        user.setPassword (register.getPassword ());
        user.setTzone (register.getTzone ());
        userRepository.save (user);

        return "redirect:/login";
    }
}

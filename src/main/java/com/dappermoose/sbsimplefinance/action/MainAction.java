package com.dappermoose.sbsimplefinance.action;

import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dappermoose.sbsimplefinance.dao.AccountRepository;
import com.dappermoose.sbsimplefinance.data.Account;
import com.dappermoose.sbsimplefinance.data.User;

// TODO: Auto-generated Javadoc
/**
 * The Class MainAction.
 */
@Controller
public class MainAction
{
    @Inject
    private AccountRepository accountRepository;

    /**
     * Main action.
     *
     * @param model
     *            - the model to display
     * @param session
     *            - the http session
     *
     * @return the string
     */
    @MustBeLoggedIn
    @Transactional (readOnly = true)
    @RequestMapping ("/main")
    public String mainAction (final Model model, final HttpSession session)
    {
        final List<Account> accounts = accountRepository.findByUser (User
                .getUserFromSession (session));
        model.addAttribute ("accounts", accounts);

        // get the overall balance.
        BigDecimal balance = BigDecimal.ZERO.setScale (2);
        for (final Account acct : accounts)
        {
            balance = balance.add (acct.getStartingBalance ().setScale (2));
        }
        model.addAttribute ("balance", balance);

        return "main";
    }
}

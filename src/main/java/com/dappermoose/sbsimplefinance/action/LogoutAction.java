package com.dappermoose.sbsimplefinance.action;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

// TODO: Auto-generated Javadoc
/**
 * The Class LoginAction.
 */
@Controller
public class LogoutAction
{

    /**
     * Process logout action.
     *
     * @param session the session
     * @return the string
     */
    @RequestMapping (value = "/logout")
    public String processLogoutAction (final HttpSession session)
    {
        session.invalidate ();
        return "redirect:/main";
    }
}

package com.dappermoose.sbsimplefinance.action;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.HandlerMapping;
import org.webjars.WebJarAssetLocator;

// TODO: Auto-generated Javadoc
/**
 * The Class WebjarsLocator.
 *
 * <p>based on code from the webjars web site</p>
 */
@Controller
public class WebjarsLocator
{

    /**
     * find webjars in a version-agnostic way.
     *
     * @param webjar - the particular webjar
     * @param request - the http request
     * @return the string
     */
    @ResponseBody
    @RequestMapping ("/webjarslocator/{webjar}/**")
    public ResponseEntity locateWebjarAsset (@PathVariable final String webjar, final HttpServletRequest request)
    {
        try
        {
            // This prefix must match the mapping path!
            String mvcPrefix = "/webjarslocator/" + webjar + "/";
            String mvcPath = (String) request.getAttribute (HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
            WebJarAssetLocator assetLocator = new WebJarAssetLocator ();
            String fullPath = assetLocator.getFullPath (webjar, mvcPath.substring (mvcPrefix.length ()));
            return new ResponseEntity (new ClassPathResource (fullPath), HttpStatus.OK);
        }
        catch (Exception e)
        {
            return new ResponseEntity<> (HttpStatus.NOT_FOUND);
        }
    }
}

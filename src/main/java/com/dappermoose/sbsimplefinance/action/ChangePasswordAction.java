package com.dappermoose.sbsimplefinance.action;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dappermoose.sbsimplefinance.dao.UserRepository;
import com.dappermoose.sbsimplefinance.data.User;
import com.dappermoose.sbsimplefinance.formbean.ChangePwd;
import com.dappermoose.sbsimplefinance.validation.ValidationResponse;

// TODO: Auto-generated Javadoc
/**
 * The Class ChangePasswordAction.
 */
@Controller
public class ChangePasswordAction
{

    /** The user repository. */
    @Inject
    private UserRepository userRepository;

    /** The message source. */
    @Inject
    private MessageSource messageSource;
    
    /** the time zones. */
    @Inject
    private ApplicationContext context;

    /**
     * Main action. Display the form.
     *
     * @param model the model
     * @param session the HttpSession
     * @return the string
     */
    @MustBeLoggedIn
    @RequestMapping (value = "/changepwd", method = RequestMethod.GET)
    public String mainAction (final Model model, final HttpSession session)
    {
        final ChangePwd changepwd = new ChangePwd ();
        final User user = (User) (session
                .getAttribute (User.USER_SESSION_ATTRIBUTE_NAME));
        changepwd.setUserName (user.getUserName ());
        changepwd.setTzone (user.getTzone ());

        model.addAttribute ("changepwd", changepwd);
        model.addAttribute ("tzones", context.getBean ("tzones"));
        return "changepwd";
    }

    /**
     * handle the ajax validation.
     *
     * @param changepwd the change passwor data
     * @param result the result
     * @param request the request
     * @return the validation response
     */
    @RequestMapping (value = "/changepwd.json")
    @ResponseBody
    public ValidationResponse validationAction (
            @RequestBody @Valid final ChangePwd changepwd,
            final BindingResult result, final HttpServletRequest request)
    {
        final ValidationResponse resp = new ValidationResponse ();
        if (!result.hasErrors ())
        {
            // check the current password, etc.
            final HttpSession session = request.getSession ();
            final User user = (User) (session
                    .getAttribute (User.USER_SESSION_ATTRIBUTE_NAME));
            if (!user.checkpw (changepwd.getCurrentPassword ()))
            {
                final ObjectError objErr = new ObjectError ("User",
                        messageSource.getMessage ("changepwd.currpwd.bad",
                                null, request.getLocale ()));
                result.addError (objErr);
            }
        }
        if (!result.hasErrors ())
        {
            resp.setStatus ("SUCCESS");
        }
        else
        {
            resp.setStatus ("FAIL");
            resp.setErrorMessageList (result.getAllErrors ());
        }

        return resp;
    }

    /**
     * Process change password action.
     *
     * @param changepwd the change password form data
     * @param res the res
     * @param model the model
     * @param request the request
     * @return the string
     */
    @Transactional
    @NeedsCSRFCheck
    @MustBeLoggedIn
    @RequestMapping (value = "/changepwd", method = RequestMethod.POST)
    public String processRegisterAction (
            @Valid @ModelAttribute ("changepwd") final ChangePwd changepwd,
            final BindingResult res, final Model model,
            final HttpServletRequest request)
    {
        if (res.hasErrors ())
        {
            model.addAttribute ("changepwd", changepwd);
            return "changepwd";
        }

        // check the current password, etc.
        final HttpSession session = request.getSession ();
        final User user = (User) (session
                .getAttribute (User.USER_SESSION_ATTRIBUTE_NAME));
        if (!user.checkpw (changepwd.getCurrentPassword ()))
        {
            final ObjectError objErr = new ObjectError ("User",
                    messageSource.getMessage ("changepwd.currpwd.bad", null,
                            request.getLocale ()));
            res.addError (objErr);
            model.addAttribute ("changepwd", changepwd);
            return "changepwd";
        }

        // modify user bean
        user.setPassword (changepwd.getPassword ());
        user.setTzone (changepwd.getTzone ());
        userRepository.save (user);
        session.setAttribute (User.USER_SESSION_ATTRIBUTE_NAME, user);

        return "redirect:/main";
    }
}

package com.dappermoose.sbsimplefinance.action;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.theme.SessionThemeResolver;

import com.dappermoose.sbsimplefinance.dao.LoginEventRepository;
import com.dappermoose.sbsimplefinance.dao.UserRepository;
import com.dappermoose.sbsimplefinance.data.LoginEvent;
import com.dappermoose.sbsimplefinance.data.User;
import com.dappermoose.sbsimplefinance.data.YesNoEnum;
import com.dappermoose.sbsimplefinance.formbean.LoginUser;
import com.dappermoose.sbsimplefinance.validation.ValidationResponse;

// TODO: Auto-generated Javadoc
/**
 * The Class LoginAction.
 */
@Controller
public class LoginAction
{
    @Inject
    private UserRepository userRepository;

    @Inject
    private LoginEventRepository loginEventRepository;

    @Inject
    private MessageSource messageSource;

    /**
     * Main action. Display the form.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping (value = "/login", method = RequestMethod.GET)
    public String mainAction (final Model model)
    {
        model.addAttribute ("login", new LoginUser ());
        return "login";
    }

    /**
     * handle the ajax validation.
     *
     * @param loginUser the login user
     * @param result the result
     * @param request the request
     * @return the validation response
     */
    @RequestMapping (value = "/login.json")
    @ResponseBody
    public ValidationResponse validationAction (
            @RequestBody @Valid final LoginUser loginUser,
            final BindingResult result, final HttpServletRequest request)
    {
        final ValidationResponse resp = new ValidationResponse ();
        if (!result.hasErrors ())
        {
            // check if the user exists and the password is OK
            final List<User> users = userRepository.findByUserName (loginUser
                    .getUserName ());
            if (users.size () != 1)
            {
                final ObjectError objErr = new ObjectError ("User",
                        messageSource.getMessage ("login.combo.bad", null,
                                request.getLocale ()));
                result.addError (objErr);
            }
            else
            {
                final User user = users.get (0);
                if (!user.checkpw (loginUser.getPassword ()))
                {
                    final ObjectError objErr = new ObjectError ("User",
                            messageSource.getMessage ("login.combo.bad", null,
                                    request.getLocale ()));
                    result.addError (objErr);

                    final LoginEvent event = new LoginEvent ();
                    event.setUser (user);
                    event.setSuccess (YesNoEnum.NO);
                    loginEventRepository.save (event);
                }
            }
        }
        if (result.hasErrors ())
        {
            resp.setStatus ("FAIL");
            resp.setErrorMessageList (result.getAllErrors ());
        }
        else
        {
            resp.setStatus ("SUCCESS");
        }
        return resp;
    }

    /**
     * Process login action.
     *
     * @param login the login
     * @param res the res
     * @param model the model
     * @param request the request
     * @return the string
     */
    @Transactional
    @NeedsCSRFCheck
    @RequestMapping (value = "/login", method = RequestMethod.POST)
    public String processLoginAction (
            @Valid @ModelAttribute ("login") final LoginUser login,
            final BindingResult res, final Model model,
            final HttpServletRequest request)
    {
        if (res.hasErrors ())
        {
            model.addAttribute ("login", login);
            return "login";
        }

        // check if the user already exists
        final List<User> users = userRepository.findByUserName (login
                .getUserName ());
        if (users.size () != 1)
        {
            final ObjectError objErr = new ObjectError ("User",
                    messageSource.getMessage ("login.combo.bad", null,
                            request.getLocale ()));
            res.addError (objErr);
            model.addAttribute ("login", login);
            return "login";
        }

        final User user = users.get (0);
        if (!user.checkpw (login.getPassword ()))
        {
            final ObjectError objErr = new ObjectError ("User",
                    messageSource.getMessage ("login.combo.bad", null,
                            request.getLocale ()));
            res.addError (objErr);
            model.addAttribute ("login", login);

            final LoginEvent event = new LoginEvent ();
            event.setUser (user);
            event.setSuccess (YesNoEnum.NO);
            loginEventRepository.save (event);
            return "login";
        }

        final LoginEvent event = new LoginEvent ();
        event.setUser (user);
        event.setSuccess (YesNoEnum.YES);
        loginEventRepository.save (event);

        // login is successful, kill the session and make a new one, and plop
        // the user record in it.
        HttpSession session = request.getSession ();
        // grab the theme
        final Object theme = session
                .getAttribute (SessionThemeResolver.THEME_SESSION_ATTRIBUTE_NAME);
        session.invalidate ();
        session = request.getSession (true);
        session.setAttribute (User.USER_SESSION_ATTRIBUTE_NAME, user);
        if (theme != null)
        {
            session.setAttribute (
                    SessionThemeResolver.THEME_SESSION_ATTRIBUTE_NAME, theme);
        }

        return "redirect:/main";
    }
}

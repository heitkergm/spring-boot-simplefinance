package com.dappermoose.sbsimplefinance.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.dappermoose.sbsimplefinance.data.User;

// TODO: Auto-generated Javadoc
/**
 * The Interface UserRepository.
 */
public interface UserRepository extends CrudRepository<User, Long>
{

    /**
     * Find by user name.
     *
     * @param userName the user name
     * @return the list
     */
    List<User> findByUserName (String userName);
}

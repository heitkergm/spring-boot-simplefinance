package com.dappermoose.sbsimplefinance.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.dappermoose.sbsimplefinance.data.Account;
import com.dappermoose.sbsimplefinance.data.User;

// TODO: Auto-generated Javadoc
/**
 * The Interface AccountRepository.
 */
public interface AccountRepository extends CrudRepository<Account, Long>
{

    /**
     * Find by user.
     *
     * @param user the user
     * @return the list
     */
    List<Account> findByUser (User user);
}

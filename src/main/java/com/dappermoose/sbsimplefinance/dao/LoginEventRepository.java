package com.dappermoose.sbsimplefinance.dao;

import org.springframework.data.repository.CrudRepository;

import com.dappermoose.sbsimplefinance.data.LoginEvent;

/**
 * The Interface LoginEventRepository.
 */
public interface LoginEventRepository extends CrudRepository<LoginEvent, Long>
{
}

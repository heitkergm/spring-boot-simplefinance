package com.dappermoose.sbsimplefinance.errors;

// TODO: Auto-generated Javadoc
/**
 * The Class CSRFError.
 */
public class CSRFError extends Exception
{

    /** serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new CSRF error.
     *
     * @param message the message
     */
    public CSRFError (final String message)
    {
        super (message);
    }
}

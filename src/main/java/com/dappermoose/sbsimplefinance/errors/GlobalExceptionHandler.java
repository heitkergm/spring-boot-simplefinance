package com.dappermoose.sbsimplefinance.errors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

// TODO: Auto-generated Javadoc
/**
 * The Class GlobalExceptionHandler.
 */
@ControllerAdvice
public class GlobalExceptionHandler
{

    /** The Constant DEFAULT_ERROR_VIEW. */
    public static final String DEFAULT_ERROR_VIEW = "genericError";

    /**
     * Default error handler.
     *
     * @param req
     *            the request
     * @param e
     *            the exception
     * @return the model and view
     * @throws Exception
     *             the exception
     */
    @ExceptionHandler (value = Exception.class)
    public ModelAndView defaultErrorHandler (final HttpServletRequest req,
            final Exception e) throws Exception
    {
        // If the exception is annotated with @ResponseStatus rethrow it and let
        // the framework handle it - like the OrderNotFoundException example
        // at the start of this post.
        // AnnotationUtils is a Spring Framework utility class.
        if (AnnotationUtils
                .findAnnotation (e.getClass (), ResponseStatus.class) != null)
        {
            throw e;
        }

        // Otherwise setup and send the user to a default error-view.
        final ModelAndView mav = new ModelAndView ();
        mav.addObject ("exception", e);
        mav.addObject ("url", req.getRequestURL ());
        mav.setViewName (DEFAULT_ERROR_VIEW);
        return mav;
    }
}

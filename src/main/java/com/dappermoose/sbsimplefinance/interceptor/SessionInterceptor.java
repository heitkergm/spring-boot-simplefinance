package com.dappermoose.sbsimplefinance.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

// TODO: Auto-generated Javadoc
/**
 * The Class SessionInterceptor.
 */
public class SessionInterceptor extends HandlerInterceptorAdapter
{

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.web.servlet.handler.HandlerInterceptorAdapter#preHandle
     * (javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse, java.lang.Object)
     */
    @Override
    public boolean preHandle (final HttpServletRequest request,
            final HttpServletResponse response, final Object handler)
            throws Exception
    {
        final boolean retVal = true;

        request.getSession (true);
        return retVal;
    }
}

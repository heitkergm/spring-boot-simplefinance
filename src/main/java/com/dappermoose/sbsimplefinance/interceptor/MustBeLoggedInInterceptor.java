package com.dappermoose.sbsimplefinance.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.dappermoose.sbsimplefinance.action.MustBeLoggedIn;
import com.dappermoose.sbsimplefinance.data.User;

// TODO: Auto-generated Javadoc
/**
 * The Class MustBeLoggedInInterceptor.
 */
public class MustBeLoggedInInterceptor extends HandlerInterceptorAdapter
{

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.web.servlet.handler.HandlerInterceptorAdapter#preHandle
     * (javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse, java.lang.Object)
     */
    @Override
    public boolean preHandle (final HttpServletRequest request,
            final HttpServletResponse response, final Object handler)
            throws Exception
    {
        boolean retVal = true;

        if (handler instanceof HandlerMethod)
        {
            final HandlerMethod method = (HandlerMethod) handler;
            if (method.getMethodAnnotation (MustBeLoggedIn.class) != null)
            {
                final HttpSession session = request.getSession ();
                final User user = (User) (session
                        .getAttribute (User.USER_SESSION_ATTRIBUTE_NAME));

                if (user == null)
                {
                    retVal = false;
                    response.sendRedirect (request.getContextPath () + "/login");
                }
            }
        }
        return retVal;
    }
}

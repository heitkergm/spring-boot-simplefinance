package com.dappermoose.sbsimplefinance.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

// TODO: Auto-generated Javadoc
/**
 * The Class AddResponseHeadersInterceptor.
 */
public class AddResponseHeadersInterceptor extends HandlerInterceptorAdapter
{
    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.web.servlet.handler.HandlerInterceptorAdapter#preHandle
     * (javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse, java.lang.Object)
     */
    @Override
    public boolean preHandle (final HttpServletRequest request,
            final HttpServletResponse response, final Object handler)
                    throws Exception
    {
        // anti-clickjack
        response.addHeader ("X-Frame-Options", "DENY");

        // prevent caching
        // HTTP 1.1.
        response.setHeader ("Cache-Control",
                "no-cache, no-store, must-revalidate");
        // HTTP 1.0.
        response.setHeader ("Pragma", "no-cache");
        // Proxies.
        response.setDateHeader ("Expires", 0);

        return true;
    }
}

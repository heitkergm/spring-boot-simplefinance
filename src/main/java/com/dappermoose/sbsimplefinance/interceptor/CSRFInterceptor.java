package com.dappermoose.sbsimplefinance.interceptor;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.MessageSource;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.dappermoose.sbsimplefinance.action.NeedsCSRFCheck;
import com.dappermoose.sbsimplefinance.errors.CSRFError;
import com.dappermoose.sbsimplefinance.util.CSRFToken;

// TODO: Auto-generated Javadoc
/**
 * The Class CSRFInterceptor.
 */
public class CSRFInterceptor extends HandlerInterceptorAdapter
{
    // this class MUST be a bean, so injection works.
    @Inject
    private MessageSource messageSource;

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.web.servlet.handler.HandlerInterceptorAdapter#preHandle
     * (javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse, java.lang.Object)
     */
    @Override
    public boolean preHandle (final HttpServletRequest request,
            final HttpServletResponse response, final Object handler)
                    throws Exception
    {
        final boolean retVal = true;

        if (handler instanceof HandlerMethod)
        {
            final HandlerMethod method = (HandlerMethod) handler;
            if (method.getMethodAnnotation (NeedsCSRFCheck.class) != null)
            {
                final String inputValue = request
                        .getParameter (CSRFToken.CSRF_FIELD_NAME);
                if ((inputValue == null)
                        || !inputValue.equals (CSRFToken.getToken (request
                                .getSession ())))
                {
                    final String message = messageSource.getMessage (
                            "CSRF.bad", null, request.getLocale ());
                    throw new CSRFError (message);
                }
            }
        }

        return retVal;
    }
}
